package com.example.mobilecodingchallenge.utils;

public class Const {

    //Here is all constants that we used in the app
    public static final String CURRENT_USER_NAME = "username";
    public static final String CURRENT_USER_EMAIL = "email";
    public static final String API_BASE_URL = "https://api.github.com";
    public static final String API_SEARCH_REPO = "/search/repositories?q=created:>2017-10-22&sort=stars&order=desc";

}
