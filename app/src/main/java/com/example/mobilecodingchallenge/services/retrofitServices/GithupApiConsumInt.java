package com.example.mobilecodingchallenge.services.retrofitServices;

import com.example.mobilecodingchallenge.models.Response;

import retrofit2.Call;

public interface GithupApiConsumInt {

    Call<Response> getRepos();

    Call<Response> getRepoFromPage(int numberPage);
}
