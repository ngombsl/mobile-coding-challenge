package com.example.mobilecodingchallenge.services.FirebaseServices;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.mobilecodingchallenge.R;
import com.example.mobilecodingchallenge.activities.LoginActivity;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseServiceImpl implements FirebaseServiceInt {

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private Context context;

    public FirebaseServiceImpl(Context context){
        this.context = context;
        init();
    }


    private void init(){

        if (this.mAuth == null) this.mAuth = FirebaseAuth.getInstance();
    }

    //For getting the current user information
    @Override
    public FirebaseUser getCurrentUser() {
         this.currentUser = mAuth.getCurrentUser();
        return currentUser;
    }

    //return a task for sign up the user into firebase
    @Override
    public Task<AuthResult> signUpUser(String email, String password) {
        return mAuth.createUserWithEmailAndPassword(email, password);

    }

    //return a task for sign in the user into firebase
    @Override
    public Task<AuthResult> signInUser(String email, String password) {

       return mAuth.signInWithEmailAndPassword(email, password);

    }

    //log out the user and send it to the login page
    @Override
    public void logout() {
        FirebaseAuth.getInstance().signOut();
        sendToLogin();
    }

    //send the user to the login page
    private void sendToLogin() {
        Intent loginIntent = new Intent(context, LoginActivity.class);
        Activity activity = (Activity) context;
        activity.startActivity(loginIntent);
        activity.overridePendingTransition(R.anim.pull_in_right, R.anim.pull_out_left);
        activity.finish();

    }

}
