package com.example.mobilecodingchallenge.listeners;

import android.app.Activity;
import android.view.View;

import com.example.mobilecodingchallenge.R;
import com.example.mobilecodingchallenge.activities.MainActivity;
import com.example.mobilecodingchallenge.services.FirebaseServices.FirebaseServiceImpl;

public class MainListener implements View.OnClickListener {

    private FirebaseServiceImpl firebaseService;
    private MainActivity mainActivity;
    public MainListener(FirebaseServiceImpl firebaseService, MainActivity mainActivity){
        this.firebaseService = firebaseService;
        this.mainActivity = mainActivity;
    }

    //logout the user into firebase
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.iv_logout){
            this.firebaseService.logout();
        }
        else{

            this.mainActivity.getRepos();
        }


    }
}
