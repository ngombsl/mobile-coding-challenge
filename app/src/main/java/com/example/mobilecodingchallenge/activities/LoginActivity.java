package com.example.mobilecodingchallenge.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.mobilecodingchallenge.R;
import com.example.mobilecodingchallenge.listeners.ConnectionListener;
import com.example.mobilecodingchallenge.services.FirebaseServices.FirebaseServiceImpl;

public class LoginActivity extends AppCompatActivity {

    private FirebaseServiceImpl firebaseService;

  /*  private EditText etEmail;
    private EditText etPassword;*/
    private Button btnSignin;
    private LinearLayout llDoNotHaveAccount;

    //this is the listener for evrything that turn arround connection
    //login and register
    private ConnectionListener connectionListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //bind the views with the layout
        //etEmail = findViewById(R.id.et_email);
        //etPassword = findViewById(R.id.et_password);
        btnSignin = findViewById(R.id.btn_signin);
        llDoNotHaveAccount = findViewById(R.id.ll_do_not_have_account);

        //Pass the context so to our firebase service
        firebaseService = new FirebaseServiceImpl(this);

        setupListener();
    }

    //Listener that will proceed everyt click that happen in the login view
    private void setupListener() {
        connectionListener = new ConnectionListener(getApplicationContext(),this,firebaseService);
        btnSignin.setOnClickListener(connectionListener);
        llDoNotHaveAccount.setOnClickListener(connectionListener);
    }


}
