package com.example.mobilecodingchallenge.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.mobilecodingchallenge.R;
import com.example.mobilecodingchallenge.adaptaters.GithubRepoAdaptater;
import com.example.mobilecodingchallenge.listeners.MainListener;
import com.example.mobilecodingchallenge.models.GithubRepo;
import com.example.mobilecodingchallenge.models.Response;
import com.example.mobilecodingchallenge.services.CustomDialogService;
import com.example.mobilecodingchallenge.services.FirebaseServices.FirebaseServiceImpl;
import com.example.mobilecodingchallenge.services.retrofitServices.GithubApiConsumImpl;
import com.example.mobilecodingchallenge.utils.SharedPrefs;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {

   // private TextView tvUsername;
    private TextView tvEmail;
    private ImageView ivLogout;
    private ImageView ivReload;

    private FirebaseServiceImpl firebaseService;
    private FirebaseUser firebaseUser;

    private SharedPrefs sharedPrefs;

    private MainListener mainListener;

    private ArrayList<GithubRepo> githubRepos;

    private GithubApiConsumImpl githubApiConsum;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter githubRepoAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private CustomDialogService dialogService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setup the views
        setupView();
        //init realm servcice
        inti();
        //setup out listener
        setupListenr();


    }

    private void setupRecycleView() {

        //setup the dialog

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        Toast.makeText(this, "lengh "+githubRepos.size(), Toast.LENGTH_SHORT).show();
        githubRepoAdapter =new GithubRepoAdaptater(githubRepos,this);
        recyclerView.setAdapter(githubRepoAdapter);

    }

    public void getRepos() {

        dialogService.showProgress("Loading");

        githubApiConsum.getRepos().enqueue(new Callback<Response>() {
               @Override
               public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {


                   githubRepos = response.body().getGithubRepos();
                   //setup our recycle view
                   setupRecycleView();
                   dialogService.dismissDialod();
               }

               @Override
               public void onFailure(Call<Response> call, Throwable t) {

                   dialogService.dismissDialod();
                   dialogService.errorMessage("Getting Repos",t.getMessage());
               }
        });
    }


    private void inti() {

        firebaseService = new FirebaseServiceImpl(this);
        sharedPrefs = new SharedPrefs(this);

        dialogService = new CustomDialogService(this);

        githubRepos = new ArrayList<>();
        githubApiConsum = new GithubApiConsumImpl();
    }

    private void setupView() {

        tvEmail = findViewById(R.id.tv_email);
       // tvUsername = findViewById(R.id.tv_username);
        ivLogout = findViewById(R.id.iv_logout);
        ivReload = findViewById(R.id.iv_reload);
        recyclerView = findViewById(R.id.rv_github_repo);
    }

    private void setupListenr() {
        mainListener = new MainListener(firebaseService,MainActivity.this);
        ivLogout.setOnClickListener(mainListener);
        ivReload.setOnClickListener(mainListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
       firebaseUser = firebaseService.getCurrentUser();

       if (firebaseUser == null) {

           //no user authenticate
           sendToLoginActivity();
       }
       else{

           setupUser();
       }
    }

    //setup the user informations
    private void setupUser() {
       String email = sharedPrefs.getUserEmail();
       tvEmail.setText(email);
        getRepos();
    }

    private void sendToLoginActivity() {

        //send to user to the login page for authentication
        Intent loginIntent = new Intent(MainActivity.this , LoginActivity.class);
        startActivity(loginIntent);
        overridePendingTransition(R.anim.pull_out_left, R.anim.pull_out_right);
        finish();
    }

    //load the following page and notify the adaptater
    public void loadPageNumber(int nextPage, int position) {
        dialogService.showProgress("Loading page " + nextPage);

        githubApiConsum.getRepoFromPage(nextPage).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                githubRepos.addAll(response.body().getGithubRepos());
                dialogService.dismissDialod();

                //notify the adaptater
                githubRepoAdapter.notifyItemChanged(position , githubRepos.size());

                //increment the number of page
                GithubRepoAdaptater.nextPage ++;

            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

                dialogService.dismissDialod();
                dialogService.errorMessage("Getting Repos",t.getMessage());

            }
        });
    }
}


