package com.example.mobilecodingchallenge.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.mobilecodingchallenge.R;
import com.example.mobilecodingchallenge.listeners.ConnectionListener;
import com.example.mobilecodingchallenge.services.FirebaseServices.FirebaseServiceImpl;

public class RegisterActivity extends AppCompatActivity {

   //our firebase service
    private FirebaseServiceImpl firebaseService;

    //the views
    private Button btnSignup;
    private LinearLayout llHaveAnAccount;

    //Connecttion Listener
    private ConnectionListener connectionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //bind the views with our layout
        btnSignup = findViewById(R.id.btn_signup);
        llHaveAnAccount = findViewById(R.id.ll_have_an_account);

        //dependencie injection to prepare our firebase services .
        firebaseService = new FirebaseServiceImpl(this);
        setupListener();

    }

    //config the listener for our views
    private void setupListener() {
        connectionListener = new ConnectionListener(getApplicationContext(),
                this,firebaseService);
        btnSignup.setOnClickListener(connectionListener);
        llHaveAnAccount.setOnClickListener(connectionListener);
    }
}
