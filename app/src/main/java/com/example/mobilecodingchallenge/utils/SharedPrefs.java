package com.example.mobilecodingchallenge.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static com.example.mobilecodingchallenge.utils.Const.CURRENT_USER_EMAIL;
import static com.example.mobilecodingchallenge.utils.Const.CURRENT_USER_NAME;

//sometimes it may happen that we want to use the shareprefs
//to save some kind of data to have beter performance in our app
public class SharedPrefs  {

    private Context context;
    private SharedPreferences pref;

    public SharedPrefs(Context context){
        this.context = context;
        init();
    }

    private void init() {
         pref = context.getSharedPreferences("MyPref", 0);
    }

    //set the username and the email of the user
    public void setUser(String username , String email){

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(CURRENT_USER_NAME,  username);
        editor.putString(CURRENT_USER_EMAIL,  email);
        editor.commit();

    }

    //methods to get the username and the email of the user
    public String getUserName(){
     return pref.getString(CURRENT_USER_NAME,null);
    }
    public String getUserEmail(){
        return pref.getString(CURRENT_USER_EMAIL,null);
    }


}
