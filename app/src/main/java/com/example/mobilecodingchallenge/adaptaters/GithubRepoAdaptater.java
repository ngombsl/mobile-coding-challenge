package com.example.mobilecodingchallenge.adaptaters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mobilecodingchallenge.R;
import com.example.mobilecodingchallenge.activities.MainActivity;
import com.example.mobilecodingchallenge.models.GithubRepo;
import com.example.mobilecodingchallenge.models.Owner;
import com.example.mobilecodingchallenge.models.Response;
import com.example.mobilecodingchallenge.services.retrofitServices.GithubApiConsumImpl;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class GithubRepoAdaptater extends RecyclerView.Adapter<GithubRepoAdaptater.MyViewHolder> {

    private List <GithubRepo> githubRepos;
    private Context context;
    private MainActivity mainActivity;
    public static int nextPage = 2;


    // Provide a suitable constructor
    public GithubRepoAdaptater(ArrayList<GithubRepo> githubRepos, Context context){
        this.githubRepos = githubRepos;
        this.context = context;
        mainActivity = (MainActivity)context;

    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public GithubRepoAdaptater.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.github_repo_item,parent,false);
        return new MyViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull GithubRepoAdaptater.MyViewHolder holder, int position) {
        holder.setViewValues(this.githubRepos.get(position));

        //implement the " infinity scroll bonus "
        if(position == getItemCount() -2){

            goToNextPage(nextPage,position);
        }

    }

    //called when we want to go to next page
    private void goToNextPage(int nextPage, int position) {

        mainActivity.loadPageNumber(nextPage,position);
    }

    private void loadUserAvatar(CircleImageView civAvatar, String userAvatar) {

        Picasso.get().load(userAvatar).into(civAvatar);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return githubRepos.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    public class MyViewHolder extends RecyclerView.ViewHolder {

        //relative to the user
        private CircleImageView civAvatar;
        private TextView tvType;
        private TextView tvUsernameOwner;

        //relative to the repo
        private TextView tvGithupRepoName;
        private TextView tvGithupRepoDesc;
        private TextView tvEyeCount;
        private TextView tvStarCount;
        private TextView tvScore;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            setupViews(itemView);
        }

        private void setupViews(View itemView) {
            civAvatar = itemView.findViewById(R.id.civ_avatar);
            tvUsernameOwner = itemView.findViewById(R.id.tv_username_owner);
            tvType = itemView.findViewById(R.id.tv_type);
            tvGithupRepoName = itemView.findViewById(R.id.tv_repo_name);
            tvGithupRepoDesc = itemView.findViewById(R.id.tv_repo_description);
            tvEyeCount = itemView.findViewById(R.id.tv_eyes_count);
            tvStarCount = itemView.findViewById(R.id.tv_star_count);
            tvScore = itemView.findViewById(R.id.tv_score);
        }

        //Set values for the viewHolder
        public void setViewValues(GithubRepo githubRepo) {
            Owner owner = githubRepo.getOwner();
            String userAvatar = owner.getAvatarUrl();

            //load the user avatar into the circleImageView
            loadUserAvatar(this.civAvatar,userAvatar);

            Float eyeCount = Float.valueOf(githubRepo.getWatchersCount())/1000;
            Float starCount = Float.valueOf(githubRepo.getStargazersCount())/1000;

            String eyeCountStr = String.format("%.2f k",eyeCount);
            String starCountStr = String.format("%.2f k",starCount) ;

            this.tvUsernameOwner.setText(owner.getLogin());
            this.tvType.setText(owner.getType());

            this.tvGithupRepoName.setText(githubRepo.getName());
            this.tvGithupRepoDesc.setText(githubRepo.getDescription());
            this.tvEyeCount.setText(eyeCountStr);
            this.tvStarCount.setText(starCountStr);
            this.tvScore.setText(githubRepo.getScore().toString());


        }

        //round the value to avoid long number presentation
        private double roundValue(double value) {


            DecimalFormat format = new DecimalFormat("##.0");

            return Math.round(Float.parseFloat(format.format(value)));
        }
    }
}
