package com.example.mobilecodingchallenge.services;

import android.app.Activity;
import android.graphics.Color;
import android.widget.Toast;

import com.example.mobilecodingchallenge.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CustomDialogService {

    private Activity activity;
    private SweetAlertDialog sweetAlertDialog;
    public CustomDialogService(Activity activity){
        this.activity = activity;
    }


    //show a progress while treating informations
    public void showProgress(String message) {

        sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        sweetAlertDialog.setTitleText(message);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
    }

    //display error in the case that it happens
    public void errorMessage(String title, String message) {

        if(this.activity == null )
            Toast.makeText(activity, "Context nulll", Toast.LENGTH_SHORT).show();

        sweetAlertDialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setCustomImage(R.drawable.ic_close_red_24dp)
                .setContentText(message);

        sweetAlertDialog.setCancelable(true);
        sweetAlertDialog.show();
    }

    //display warning message in the case that it happens
    public void warningMessage(String title, String message) {
        new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .show();
    }

    //display a success message to notify the user to an event
    public void successMssage(String title, String message){

        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setCustomImage(R.drawable.ic_check_green_24dp)
                .setContentText(message)
                .show();
    }

    //hide the dialog
    public void dismissDialod() {
        if (sweetAlertDialog != null) {
            sweetAlertDialog.dismiss();
        }
    }

    //sometimes we can need to change if the user can cancel
    //the dialog by typing outside of the dialog
    public void setCancellable(boolean bool){

        if(!bool) sweetAlertDialog.setCancelable(false);
    }
}
