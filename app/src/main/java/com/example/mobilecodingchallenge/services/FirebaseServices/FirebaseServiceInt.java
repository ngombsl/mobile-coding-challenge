package com.example.mobilecodingchallenge.services.FirebaseServices;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;

public interface FirebaseServiceInt {

    FirebaseUser
    getCurrentUser();


   Task<AuthResult> signUpUser(String email, String password);

   Task<AuthResult> signInUser(String email, String password);

    void logout();




}
