package com.example.mobilecodingchallenge.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Response {

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("incomplete_results")
    @Expose
    private Boolean incompleteResults;
    @SerializedName("items")
    @Expose
    private ArrayList<GithubRepo> githubRepos;

    /**
     * No args constructor for use in serialization
     *
     */
    public Response() {
    }

    /**
     *
     * @param incompleteResults
     * @param totalCount
     * @param githubRepos
     */

    public Response(Integer totalCount, Boolean incompleteResults, ArrayList<GithubRepo> githubRepos) {
        this.totalCount = totalCount;
        this.incompleteResults = incompleteResults;
        this.githubRepos = githubRepos;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public ArrayList<GithubRepo> getGithubRepos() {
        return githubRepos;
    }

    public void setGithubRepos(ArrayList<GithubRepo> githubRepos) {
        this.githubRepos = githubRepos;
    }
}