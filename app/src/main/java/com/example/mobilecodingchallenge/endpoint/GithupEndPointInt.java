package com.example.mobilecodingchallenge.endpoint;
import com.example.mobilecodingchallenge.models.Response;
import com.example.mobilecodingchallenge.utils.Const;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GithupEndPointInt {

    //to get the repos create after  2017-10-22
    //descending
    @GET(Const.API_SEARCH_REPO)
    Call <Response> getRepo();

    //to get a repos for a specifing page
    //descending
    @GET(Const.API_SEARCH_REPO)
    Call <Response> getRepoByPage(@Query("page") int pageNumber);

}
