package com.example.mobilecodingchallenge.services.retrofitServices;



import com.example.mobilecodingchallenge.utils.Const;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;




public class RetrofitService {

    //provide a retrofit for the api call
    public static Retrofit setupRetrofit (){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Const.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}

