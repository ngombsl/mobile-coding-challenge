package com.example.mobilecodingchallenge.listeners;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.example.mobilecodingchallenge.R;
import com.example.mobilecodingchallenge.activities.LoginActivity;
import com.example.mobilecodingchallenge.activities.MainActivity;
import com.example.mobilecodingchallenge.activities.RegisterActivity;
import com.example.mobilecodingchallenge.services.CustomDialogService;
import com.example.mobilecodingchallenge.services.FirebaseServices.FirebaseServiceImpl;
import com.example.mobilecodingchallenge.utils.SharedPrefs;
import com.example.mobilecodingchallenge.utils.validators.EmailValidator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


public class ConnectionListener implements View.OnClickListener {

    private CustomDialogService customDialogService;

    private final Activity activity;
    private  Context context;
    private String  email;
    private String password;

    private FirebaseServiceImpl firebaseService;

    private String username;
    private boolean isRegister = false;
    private EditText etUsername;
    private EditText etPassword;
    private EditText etEmail;

    private SharedPrefs sharedPrefs;


    //constructor
    public ConnectionListener(Context context, Activity activity, FirebaseServiceImpl firebaseService){
        this.context = context;
        this.firebaseService = firebaseService;
        this.activity = activity;

        customDialogService =new CustomDialogService(activity);
        setupView();

    }

    private void setupView( ) {

        etEmail = activity.findViewById(R.id.et_email);
        etPassword = activity.findViewById(R.id.et_password);

        if(activity.getLocalClassName().equals("activities.RegisterActivity")) {

            isRegister = true ; //for some use

            //Before to do that we need to be sure that the activiy is register
            //that's why this condition
            etUsername = activity.findViewById(R.id.et_username);
        }
    }

    private void showErrorMessage(String issue, String errorMessage) {
        customDialogService.dismissDialod();
        customDialogService.errorMessage(issue,errorMessage);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_signin){
            //we sign in the user
            if (validateInputs()) {
                //enable the costum Dialog
                showDialog("Sign In");

                //sign in the user
                signInUser(email,password);
            }

        }

        if(v.getId() ==  R.id.ll_do_not_have_account){
            //send the user to the register activity
            sendUsetToActivity(RegisterActivity.class,true);
        }

        if(v.getId() ==  R.id.ll_have_an_account){
            //send the User to the login Activity
            sendUsetToActivity(LoginActivity.class,true);
        }

        if(v.getId() ==  R.id.btn_signup){
            //sign up the user
            if (validateInputs()) {
                //show the dialog
                showDialog("Sign In");

                //sign up the user
                signUpUser();
            }
        }
    }

    private void showDialog(String message) {

        customDialogService.showProgress(message);

    }

    //this method does the validation of all the inout fields
    private boolean validateInputs() {

        this.email = etEmail.getText().toString();

        this.password = etPassword.getText().toString();

        if(isRegister){

            this.username = etUsername.getText().toString();

            if(TextUtils.isEmpty(this.username)) {
                showErrorMessage("Username ","Please fill a username");
                return  false;
            }
        }

        if(!EmailValidator.isValidEmail(this.email)) {
                showErrorMessage("Email","Pleaese enter a valid email");
                return false;
        }

        if (TextUtils.isEmpty(this.password)){
            showErrorMessage("Password","Please enter a valid password");
            return false;
        }

        return  true;
    }

    //Signup the user into the firebase
    private void signUpUser() {

        firebaseService.signUpUser(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){
                    great("Sign Up Succesfully ");
                }
                else {
                    showErrorMessage("Sign Up",task.getException().getMessage());
                }
            }
        });
    }


    //wehen this method is called that means everything is OK
    private void great(String succesMessage) {

        //dismiss the dialog
        customDialogService.dismissDialod();
        // showSuccessfulMessage(succesMessage);
        //save the user information in sharepreferences
        //display a message for succes message
        //Send the user to the main Activity

        //this following steps happen when we have a successfull login or registerÒ
        saveUserInSharePrefs(username,email);
        sendUsetToActivity(MainActivity.class, true);
    }

    private void sendUsetToActivity(Class activityClass, boolean finishAfterSend) {

        Intent intent = new Intent(context,activityClass);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

        //animate the view
        activity.overridePendingTransition(R.anim.pull_in_right,R.anim.pull_out_left);

        //In the case we want to remove the activity in the stack such from
        //such that from Login to Main
        //we don't want the user come back to login page by pressing the back button for example
        if(finishAfterSend) activity.finish();
    }


    //sign in the user into firebase
    private void signInUser(String email, String password) {
        firebaseService.signInUser(email,password).addOnCompleteListener(
                new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful() ){

                    great("Sign in Successfully");

                }else {
                   showErrorMessage("Sign In", task.getException().getMessage());
                }

            }
        });

    }

    //Saveuser Info into the share preferences
    private void saveUserInSharePrefs(String username, String email) {

        sharedPrefs = new SharedPrefs(context);
        sharedPrefs.setUser(username,email);
    }

    //show a successfull message
    private void showSuccessfulMessage(String succeedMessage) {

        customDialogService.successMssage("",succeedMessage);

    }


}
