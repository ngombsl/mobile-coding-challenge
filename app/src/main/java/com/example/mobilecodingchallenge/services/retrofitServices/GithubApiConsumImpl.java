package com.example.mobilecodingchallenge.services.retrofitServices;

import com.example.mobilecodingchallenge.endpoint.GithupEndPointInt;
import com.example.mobilecodingchallenge.models.Response;

import retrofit2.Call;
import retrofit2.Retrofit;

public class GithubApiConsumImpl implements GithupApiConsumInt {

    private  Retrofit retrofit ;

    private GithupEndPointInt githupEndPointInt;

    //constructor that call the method init
    public GithubApiConsumImpl(){
        init();
    }

    //method init initialize once the retrofit
    public  void init(){
        if(retrofit == null )
            retrofit =  RetrofitService.setupRetrofit();
        //setup the endoint
        githupEndPointInt = retrofit.create(GithupEndPointInt.class);
    }

    //get the repos which are for a date d
    @Override
    public Call<Response> getRepos() {

       return githupEndPointInt.getRepo();

    }

    //gettting the repos by giving the page number
    @Override
    public Call<Response> getRepoFromPage(int numberPage) {

        return githupEndPointInt.getRepoByPage(numberPage);

    }
}
