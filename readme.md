# Mobile Coding Challenge implemented in Android/Java


## How to use it ?

1. Clone the project
> ```git clone https://ngombsl@bitbucket.org/ngombsl/mobile-coding-challenge.git```
>

2. Open the terminal and type the following command to Switch to develop.
> ```git checkout develop```
   
3. Pull from develop 
> ```git pull```

4. Run the project 
            
   
## Libraries 


>In this Mobile application , I mainly use: 

* FirebaseAuth just for the user authentification. 
> <https://firebase.google.com>

* Retrofit to communicate with the github API.
> https://square.github.io/retrofit/

* Sweet Alert dialog used to create custom Dialog in the app 
> https://github.com/pedant/sweet-alert-dialog

* Circle Image view 
> <https://github.com/hdodenhof/CircleImageView>


* Picasso to load the user avatar:
> https://square.github.io/picasso/

> here is a link that compare Picasso and Glide <https://medium.com/@multidots/glide-vs-picasso-930eed42b81d>



### Mouhamed Lamine Ngom

> Any feadback from you will be a pleasure cause I'm still learning. 

